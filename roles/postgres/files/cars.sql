--
-- cars_userQL database dump
--

-- Dumped from database version 14.4
-- Dumped by pg_dump version 14.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: cars; Type: TABLE; Schema: public; Owner: cars_user
--

CREATE TABLE public.cars (
    id integer NOT NULL,
    name character varying(50),
    country character varying(30),
    auto_kpp boolean
);


ALTER TABLE public.cars OWNER TO cars_user;

--
-- Name: cars_id_seq; Type: SEQUENCE; Schema: public; Owner: cars_user
--

CREATE SEQUENCE public.cars_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cars_id_seq OWNER TO cars_user;

--
-- Name: cars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cars_user
--

ALTER SEQUENCE public.cars_id_seq OWNED BY public.cars.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: cars_user
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(50),
    password character varying(50)
);


ALTER TABLE public.users OWNER TO cars_user;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: cars_user
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO cars_user;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cars_user
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: cars id; Type: DEFAULT; Schema: public; Owner: cars_user
--

ALTER TABLE ONLY public.cars ALTER COLUMN id SET DEFAULT nextval('public.cars_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: cars_user
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: cars; Type: TABLE DATA; Schema: public; Owner: cars_user
--

COPY public.cars (id, name, country, auto_kpp) FROM stdin;
7	SecondTestCar	Russia	t
2	UnkownTestCar	Germany	t
8	ThirdTestCar	Russia	f
13	FirstCarTwo	Germany	f
15	FirstCarThree	Russia	f
1	First_Car	Germany	t
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: cars_user
--

COPY public.users (id, name, password) FROM stdin;
1	Admin	29f86871a90d3d4ca5735330e079c80d
\.


--
-- Name: cars_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cars_user
--

SELECT pg_catalog.setval('public.cars_id_seq', 15, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cars_user
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: cars cars_name_key; Type: CONSTRAINT; Schema: public; Owner: cars_user
--

ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_name_key UNIQUE (name);


--
-- Name: cars cars_name_key1; Type: CONSTRAINT; Schema: public; Owner: cars_user
--

ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_name_key1 UNIQUE (name);


--
-- Name: cars cars_pkey; Type: CONSTRAINT; Schema: public; Owner: cars_user
--

ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (id);


--
-- Name: users users_name_key; Type: CONSTRAINT; Schema: public; Owner: cars_user
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_name_key UNIQUE (name);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: cars_user
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: TABLE cars; Type: ACL; Schema: public; Owner: cars_user
--

GRANT ALL ON TABLE public.cars TO cars_user;


--
-- Name: SEQUENCE cars_id_seq; Type: ACL; Schema: public; Owner: cars_user
--

GRANT ALL ON SEQUENCE public.cars_id_seq TO cars_user;


--
-- Name: TABLE users; Type: ACL; Schema: public; Owner: cars_user
--

GRANT ALL ON TABLE public.users TO cars_user;


--
-- Name: SEQUENCE users_id_seq; Type: ACL; Schema: public; Owner: cars_user
--

GRANT ALL ON SEQUENCE public.users_id_seq TO cars_user;


--
-- cars_userQL database dump complete
--

